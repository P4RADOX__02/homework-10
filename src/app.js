const calc = {
    operand1: "",
    operand2: "",
    sign: "",
    result: ""
};

window.addEventListener("DOMContentLoaded", () => {
    const btns = document.querySelector(".keys");
    btns.addEventListener("click", click)
})

const click = (e) => {
    //console.log(e.target.value)
    if (/[0-9.]/.test(e.target.value) && calc.sign === "") {
        calc.operand1 += e.target.value
        show(calc.operand1)
    } else if (/[0-9.]/.test(e.target.value) && calc.sign !== "") {
        calc.operand2 += e.target.value
        show(calc.operand2)
    } else if (/[+*-/]/.test(e.target.value)) {
        calc.sign = e.target.value
    } else if (/=/.test(e.target.value)) {
        calc.result = calculation(calc.operand1, calc.operand2, calc.sign)
        document.querySelector(".display > input").value = calc.result
    } else if(/[C]/.test(e.target.value)) {
        clear()
    }
    else {
        console.error("error")
    }
}


        
function calculation (n1, n2, sign) {
    switch(sign){
        case "+" : return parseFloat(n1) + parseFloat(n2);
        case "-" : return n1 - n2;
        case "*" : return n1 * n2;
        case "/" : return n1 / n2;
    } 
}

function clear() {
    calc.operand1 = "";
    calc.operand2 = "";
    calc.sign = ""; 
    document.querySelector(".display > input").value = "";
}

const show = (data) => {
    document.querySelector(".display > input").value = data
}
